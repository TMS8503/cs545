# Add main protobuf module to classpath
import sys
import socket

# Import required RPC modules
import interface_pb2


class SMBus(object):
    """SMBus([bus]) -> SMBus
    Return a new SMBus object that is simulated connection
    to the I2C device interface.
    """
    def __init__(self, bus=-1):
	self.gopigo_plugin_service = socket.socket()         # Create a socket object
	self.host = socket.gethostname() # Get local machine name
	self.port = 8080                # Reserve a port 

	self.gopigo_plugin_service.connect((self.host, self.port))

	self.written_address_data = {}

    def __del__(self):
        self.gopigo_plugin_service.close 

    def read_byte(self, addr):
        """read_byte(addr) -> result

        Perform SMBus Read Byte transaction.
        """
	return self.written_address_data[addr]

    def write_byte(self, addr, val):
        """write_byte(addr, val)

        Perform SMBus Write Byte transaction.
        """


    def write_i2c_block_data(self, addr, cmd, vals):
        """write_i2c_block_data(addr, cmd, vals)

        Perform I2C Block Write transaction.
        """

	command_enum = vals[0]
	message = interface_pb2.RequestMessage()
	message.cmd = command_enum
	wait_for_reply = False
	if (command_enum == interface_pb2.Interface_Command.US) or (command_enum == interface_pb2.Interface_Command.VOLT) or (command_enum == interface_pb2.Interface_Command.FW_VER):
		wait_for_reply = True
		message.expect_reply = True
	
	self.gopigo_plugin_service.send(message.SerializeToString())
	if wait_for_reply:
		while True:
			data = self.gopigo_plugin_service.recv(24)
			if data:
				#create a RespondMessage
				respond = interface_pb2.RespondMessage()
				
				# parse the message from the provided data
				respond.ParseFromString(data)
				
				# convert string to float
				range = float(respond.reply)
				
				# GoPiGo converts data from serial address to readable data by
				# using this function: v=b1*256+b2
				# we need to take our readable data and turn it into serial address looking thing
				addressData = range/257
				
				#write the data to our fake address
				self.written_address_data[addr] = addressData
				break
	







