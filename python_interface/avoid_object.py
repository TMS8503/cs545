from gopigo import *	#Has the basic functions for controlling the GoPiGo Robot
import sys	#Used for closing the running program
import time, random, logging


def search():
	while True:
		dist=us_dist(15)
		if dist < 95:
			print "Something is in font of us."
			return True
		stop()
		time.sleep(3)
		fwd()
		time.sleep(2)

			
if __name__ == '__main__':

	fwd()
	time.sleep(2)
	foundit = False;
	while not foundit:
		foundit = search()
	if foundit:
		stop()
		time.sleep(3)
		print "Turning right!"
		right()
		time.sleep(3)
		stop()
		stop()
		time.sleep(4)
		fwd()
		time.sleep(3)
		stop()
		
		
	stop()