/*
 *  GoPiGo model plugin for Gazebo
 * 
 * Credit: Initially based on the model push example from http://gazebosim.org (Open Source Robotics Foundation)
*/


#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <string>
#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>

#include "../proto/generated_files/interface.pb.h"

using std::cout;
using std::endl;
using std::string;

namespace gazebo
{

#define PORT 8080           // the port users will be connecting to
#define MAXDATASIZE 4096    // max number of bytes we can send at once
#define BACKLOG 10          // how many pending connections queue will hold

void sigchld_handler(int s) {
    while (waitpid(-1, NULL, WNOHANG) > 0);
}

// hacky fix
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}


class ModelPush : public ModelPlugin
{
	

	enum Motor_Type{
		LEFT_WHEEL_MOTOR= 0,
		RIGHT_WHEEL_MOTOR,
		MAX,
			
		INVALID = -1
			
	};
	
	

	//! starts the server
	public: void run()
	{
		int listenfd;
		int connectfd;
		int numbytes;
		char buf[MAXDATASIZE];
		struct sockaddr_in server;
		struct sockaddr_in client;
		socklen_t sin_size;
		struct sigaction sa;

		if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
		{
			gzerr << "socket error\n";
		}

		int opt = SO_REUSEADDR;
		if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) == -1) 
		{
			gzerr << "setsockopt error\n";
		}

		bzero(&server, sizeof(server));
		server.sin_family = AF_INET;                // host byte order
		server.sin_port = htons(PORT);              // short, network byte order
		server.sin_addr.s_addr = htonl(INADDR_ANY); // automatically fill with my IP

		if (bind(listenfd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1)
		{
			gzerr << "bind error\n";
		}

		if (listen(listenfd, BACKLOG) == -1)
		{
			gzerr << "listen error\n";
		}

		sa.sa_handler = sigchld_handler;  // reap all dead processes
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = SA_RESTART;
		if (sigaction(SIGCHLD, &sa, NULL) == -1) 
		{
			gzerr << "sigaction error\n";
		}
		
		while (1) 
		{     
			// main accept() loop
			sin_size = sizeof(struct sockaddr_in);

			connectfd = accept(listenfd, (struct sockaddr *)&client, &sin_size);

			while(1)
			{
				numbytes = recv(connectfd, buf, MAXDATASIZE, 0);
				buf[numbytes] = '\0';
				string a = buf;

				if(numbytes!= 0)
				{
					HandleMessage(a, connectfd);
				}
			}


			close(connectfd);
		}
		cout << "Closing"<<endl;
		close(listenfd);

	}
	
	//! manage messages received from the simulated SMBus
	public: void HandleMessage(std::string msg, int connectfd)
	{
		cout << "Handling message from smbus... "<< endl;
		// Receive a msg from clients
		gopigo::interface::RequestMessage fromClient;
		fromClient.ParseFromString(msg);
		cout << "Cmd:\t" << fromClient.cmd() << endl;
		::gopigo::interface::Interface_Command_CommandInterface command = fromClient.cmd();
		
		m_leftWheelPtr->SetDamping(0,0 );
		m_rightWheelPtr->SetDamping(0, 0);
		
		std::string returnData;

		// based on command type do something to the model in gazebo
		switch(command)
		{
			case gopigo::interface::Interface_Command::FWD:
			case gopigo::interface::Interface_Command::MOTOR_FWD:
				//Move forward
				MoveForward();
				break;
			case gopigo::interface::Interface_Command::BWD:
			case gopigo::interface::Interface_Command::MOTOR_BWD:
				//Move Backward
				 MoveBackward();
				break;
			case gopigo::interface::Interface_Command::LEFT:
				//Turn GoPiGo Left slow (one motor off, better control)	
				Turn(LEFT_WHEEL_MOTOR, false);
				break;
			case gopigo::interface::Interface_Command::LEFT_ROT:
				//Rotate GoPiGo left in same position (both motors moving in the opposite direction)
				Turn(LEFT_WHEEL_MOTOR, true);
				break;
			case gopigo::interface::Interface_Command::RIGHT:
				//Turn GoPiGo right slow (one motor off, better control)
				Turn(RIGHT_WHEEL_MOTOR, false);
				break;
			case gopigo::interface::Interface_Command::RIGHT_ROT:
				//Rotate GoPiGo left in same position (both motors moving in the opposite direction)
				Turn(RIGHT_WHEEL_MOTOR, true);
				break;
			case gopigo::interface::Interface_Command::STOP:
				//Stop the GoPiGo
				 StopMovement();
				break;
			case gopigo::interface::Interface_Command::ISPD:
				//Increase the speed by 10
				ChangeSpeed(10);
				break;
			case gopigo::interface::Interface_Command::DSPD:
				//Decrease the speed by 10
				ChangeSpeed(-10);
				break;
			case gopigo::interface::Interface_Command::US:
				returnData =  patch::to_string(ReadUltrasonicSensor());
				break;
			case gopigo::interface::Interface_Command::SET_LEFT_SPEED:
				//Set speed of the left motor
				ChangeSpeed(LEFT_WHEEL_MOTOR, 5);
				break;
			case gopigo::interface::Interface_Command::SET_RIGHT_SPEED:
				//Set speed of the right motor
				ChangeSpeed(RIGHT_WHEEL_MOTOR, 5);
				break;
			default:
				cout << "Error: Unsupported command " << command << endl;
		}
		
		bool robot_reply = false;
		if(fromClient.has_expect_reply())
		{
			robot_reply = fromClient.expect_reply();
		}
				
		if(robot_reply)
		{
			cout << "Sending reply " << command << endl;
			// Send msg to clients
			string data;
			gopigo::interface::RespondMessage to;
			to.set_cmd(command);
			if(!returnData.empty())
			{
				cout << "Returning data  " << returnData << endl;
				to.set_reply(returnData);
			}
			to.SerializeToString(&data);
			char bts[data.length()];
			sprintf(bts, "%s", data.c_str());
			cout << "Size  " << sizeof(bts) << endl;
			send(connectfd, bts, sizeof(bts), 0);
		}
	}

	void MoveForward()
	{

		if(m_robot_speed_right == 0)
		{
			m_robot_speed_right=0.5;
		}
		
		if(m_robot_speed_left==0)
		{
			m_robot_speed_left=0.5;
		}
		
		if(m_robot_speed_left < 0)
		{
			// change speed to neg
			m_robot_speed_left = m_robot_speed_left * -1;
		}
			
		if(m_robot_speed_right < 0)
		{
			// change speed to neg
			m_robot_speed_right = m_robot_speed_right * -1;
		}
		
		
		
	}
	
	void MoveBackward()
	{
		if(m_robot_speed_right == 0)
		{
			m_robot_speed_right=0.5;
		}
		
		if(m_robot_speed_left==0)
		{
			m_robot_speed_left=0.5;
		}

		if(m_robot_speed_left > 0)
		{
			// change speed to neg
			m_robot_speed_left = (m_robot_speed_left * -1)/2;
		}
			
		if(m_robot_speed_right > 0)
		{
			// change speed to neg
			m_robot_speed_right = (m_robot_speed_right * -1)/2;
		}
		
		m_robot_speed_right = 0;
		m_robot_speed_left = 0;
		
	}
	
	void Turn(Motor_Type motor, bool rot_other_motor)
	{
		if(m_robot_speed_right == 0)
		{
			m_robot_speed_right=0.5;
		}
		
		if(m_robot_speed_left==0)
		{
			m_robot_speed_left=0.5;
		}
		
		switch(motor)
		{
			case LEFT_WHEEL_MOTOR:
				if(rot_other_motor)
				{
					m_robot_speed_right = m_robot_speed_right * -1;

				}
				else
				{
					m_robot_speed_right =0;
					m_rightWheelPtr->SetStiffness (0, 1.0);
					m_rightWheelPtr->SetDamping(0, .7);
				}
				break;
			case RIGHT_WHEEL_MOTOR:
				if(rot_other_motor)
				{
					m_robot_speed_left = m_robot_speed_left * -1;
				}
				else
				{
					m_robot_speed_left=0;
					m_leftWheelPtr->SetStiffness (0, 1.0);
					m_leftWheelPtr->SetDamping(0, .7);
				}
				break;
			default:
				// no speed change
				break;

		};
		
		m_leftWheelPtr->SetStiffness (0, 0);
		m_rightWheelPtr->SetStiffness (0, 0);
	}
	
	void StopMovement()
	{
		// fake friction until we upgrade to the newer version of
		// gazebo - anything higher than 0.5 will flip the robot
		m_leftWheelPtr->SetDamping(0, 0.5);
		m_rightWheelPtr->SetDamping(0, 0.5);
		
		m_rightWheelPtr->SetStiffness (0, 2.0);
		m_leftWheelPtr->SetStiffness (0, 2.0);
		m_robot_speed_right =0;
		m_robot_speed_left =0;
		
		m_leftWheelPtr->Reset();
		m_rightWheelPtr->Reset();
		m_rightWheelPtr->SetStiffness (0, 0);
		m_leftWheelPtr->SetStiffness (0, 0);
		
	}
	
	void ChangeSpeed(float speed)
	{
		bool decrease = false;
		if(speed < 0)
		{
			decrease=true;
			speed = (decrease * -1);
		}
		
		float force = ConvertRobotSpeedToGazeboForce(speed);
		
		if(decrease)
		{
			m_robot_speed_left -=force;
			m_robot_speed_right -=force;
		}
		else
		{
			m_robot_speed_left +=force;
			m_robot_speed_right +=force;
		}

	}
	
	void ChangeSpeed(Motor_Type motor, float speed)
	{
		bool decrease = false;
		if(speed < 0)
		{
			decrease=true;
			speed = (decrease * -1);
		}
		
		float force = ConvertRobotSpeedToGazeboForce(speed);
		
		switch(motor)
		{
			case LEFT_WHEEL_MOTOR:
				if(decrease)
				{
					m_robot_speed_left -=force;
					m_leftWheelPtr->SetDamping(0, (force));
				}
				else
				{
					m_robot_speed_left +=force;
				}
				m_leftWheelPtr->SetForce(0, m_robot_speed_left);
				break;
			case RIGHT_WHEEL_MOTOR:
				if(decrease)
				{
					m_robot_speed_right -=force;
					m_rightWheelPtr->SetDamping(0, (force));
				}
				else
				{
					m_robot_speed_right +=force;
				}
				m_rightWheelPtr->SetForce(0, m_robot_speed_right);
				break;
			default:
				// no speed change
				break;
		};
		
	}
	

	double ReadUltrasonicSensor()
	{
		// the  max range of our gazebo sensor is 2.5 meters. The max range of the robots real
		// sensor is 250 cm, so we need to do some math before we send back the data
		double distance = 0;
		if(m_sonarPtr)
		{
			double gazeboRange = m_sonarPtr->GetRange();
			cout << "Gazebo Range " <<gazeboRange<<endl;
			// the sim needs a little more wiggle room to change movements so
			// make the objects seem a little closer than they are (60 vs 100)
			distance = gazeboRange * 60; 
			distance = round( distance * 1000.0 ) / 1000.0;
		}
		else
		{
				// there is a sensor attached but its not a sonar sensor
				cout << "Error: The sonar sensor is not valid." << endl;
		}
		
		cout << "GoPiGo Range " <<distance<<endl;
		return distance;
	}
	
	float ConvertRobotSpeedToGazeboForce(float smbus_reported_speed)
	{
		// speed can rnage from 0 - 255
		// 0.1 force = 12.75 smbus robot speed 
		float diff = 12.75;
		float robot_speed = (smbus_reported_speed/diff) * 0.1;
		return robot_speed;
	}
	

	// The only other mandatory function is Load which receives an SDF element that contains the elements and attributes specified in loaded SDF file.
	public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
	{
		common::Console::SetQuiet(false);

		m_robot_speed_right =0;
		m_robot_speed_left =0;
		

		// Store the pointer to the model
		this->model = _parent;

		// start up the interface server for the SMBus communication
		boost::thread* thr = new boost::thread(boost::bind(&ModelPush::run, this));

		m_leftWheelPtr = this->model->GetJoint("left_wheel_hinge");
		m_rightWheelPtr = this->model->GetJoint("right_wheel_hinge");
		      
		// Listen to the update event. This event is broadcast every simulation iteration.
		this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ModelPush::OnUpdate, this, _1));
		

		//m_sonarJointPtr = this->model->GetJoint("sonar_gopigo");
		std:;string sensorName = "sonar";
		sensors::SensorPtr sensorPtr = sensors::get_sensor(sensorName);
		if(sensorPtr)
		{
			m_sonarPtr = boost::dynamic_pointer_cast<sensors::SonarSensor>(sensorPtr);
			if(m_sonarPtr)
			{
				cout << "Loaded sonar sensor " << sensorName<< "." << endl;
			}
			else
			{
				// there is a sensor attached but its not a sonar sensor
				cout << "Error: Sensor [" << sensorName<< "] is not a sonar sensor." << endl;
			}
		}
		else
		{
			// there is no sensor connected to our robot - is something is wrong with the model?
			cout << "Error: Sensor [" << sensorName<< "] is not valid." << endl;
		}

	}

	// Called by the world update start event
	public: void OnUpdate(const common::UpdateInfo & /*_info*/)
	{
			// this should prob move somewhere so we aren't calling it all the time but for
			// now it works
			m_leftWheelPtr->SetForce(0, m_robot_speed_left);
			m_rightWheelPtr->SetForce(0, m_robot_speed_right);
	}
	
	
	// Pointer to the model
	private: physics::ModelPtr model;

	// Pointer to the update event connection
	private: event::ConnectionPtr updateConnection;
		
	
	physics::JointPtr m_leftWheelPtr;
	physics::JointPtr m_rightWheelPtr;
	physics::JointPtr m_sonarJointPtr;
	sensors::SonarSensorPtr m_sonarPtr;
	
	
	float m_robot_speed_right;
	float m_robot_speed_left;
	

	
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
  
}

