#!/usr/bin/python

# Add main protobuf module to classpath
import sys
import socket

# Import required RPC modules
import interface_pb2

# Configure logging
import logging
log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 8080                # Reserve a port 

s.connect((host, port))
# Create a request
message = interface_pb2.RequestMessage()
message.cmd = interface_pb2.Interface_Command.FWD
s.send(message.SerializeToString())
s.close
