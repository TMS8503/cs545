/*
 * Description:  test protobuf in client
 *
 * Copyright (C) 2011 lytsing.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <iostream>
#include <boost/thread/thread.hpp>
#include "../proto/generated_files/interface.pb.h"

#define HOSTNAME "localhost"
#define PORT 8080           // the port client will be connecting to
#define MAXDATASIZE 4096    // max number of bytes we can get at once

using std::cout;
using std::endl;
using std::string;

/* simple little function to write an error string and exit */
static void err(const char* s) {
    perror(s);
    exit(EXIT_FAILURE);
}

void SendServerMessage(gopigo::interface::Interface_Command_CommandInterface command, int fd)
{
    int numbytes;
    char buf[MAXDATASIZE];
    string msg;
    gopigo::interface::RequestMessage p;
    p.set_cmd(command);
    p.SerializeToString(&msg);
    sprintf(buf, "%s", msg.c_str());
    send(fd, buf, sizeof(buf), 0);

    numbytes = recv(fd, buf, MAXDATASIZE, 0);
    buf[numbytes] = '\0';
    string data = buf;
    gopigo::interface::RespondMessage from;
    from.ParseFromString(data);
    cout << "Cmd:\t"    << from.cmd() << endl;
}

int main(int argc, char** argv) {
    int fd;
    int numbytes;
    char buf[MAXDATASIZE];
    struct hostent *he;
    struct sockaddr_in server;

    if ((he = gethostbyname(HOSTNAME)) == NULL) {
        err("gethostbyname");
    }

    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        err("socket");
    }

    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    server.sin_addr = *((struct in_addr *)he->h_addr);

    if (connect(fd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1) {
        err("connect");
    }

   //SendServerMessage(gopigo::interface::Interface_Command::FWD, fd);
   //boost::this_thread::sleep( boost::posix_time::seconds(5) );
  // SendServerMessage(gopigo::interface::Interface_Command::BWD, fd);
	   //boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::LEFT, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::RIGHT, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::BWD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
    	SendServerMessage(gopigo::interface::Interface_Command::RIGHT_ROT, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
    	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::LEFT_ROT, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	
	
	//flip it!!!!
	
	SendServerMessage(gopigo::interface::Interface_Command::FWD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
	SendServerMessage(gopigo::interface::Interface_Command::ISPD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(1) );
	SendServerMessage(gopigo::interface::Interface_Command::ISPD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(1) );
	SendServerMessage(gopigo::interface::Interface_Command::ISPD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(1) );
	SendServerMessage(gopigo::interface::Interface_Command::ISPD, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(1) );
	SendServerMessage(gopigo::interface::Interface_Command::STOP, fd);
	boost::this_thread::sleep( boost::posix_time::seconds(5) );
    

    close(fd);
    return 0;
}
